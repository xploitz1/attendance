<?php

namespace App\Http\Controllers;
use App\Attendance;
use App\Models\User;
use App\Http\Resources\AttendanceResource;
use App\Http\Resources\AttendanceResourceCollection;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Carbon\Carbon;
use stdClass;
use Carbon\CarbonInterval;
use DateTime;

class AttendanceController extends Controller
{
    /**
     * @param Attendance $attendance
     * @return AttendanceResource 
     */
    public function show(Request $request)
    {   
        $attendance = Attendance::find($request->id);
        if(is_null($attendance)){
            return response()->json('Attendance not found!', 404);
        }   
        $attendance = new AttendanceResource($attendance);
        return response()->json($attendance, 201);
    }
    

    public function index(Request $request)
    {
        $limit = 10;

        if(isset($request->limit)) $limit = $request->limit;

        $attendance = Attendance::orderBy('created_at', 'desc')->paginate($limit);
        $attendance = new AttendanceResourceCollection($attendance);
        return response()->json($attendance, 201);
    }
    /**
     * @param Request $request
     * @return AttendanceResource
     */

    public function store(Request $request)
    {
        $request->validate([
            'employee_id' => 'required',
            
        ]);
        
        $user = User::where('employee_id', $request->employee_id)->first();
        if(is_null($user)){
            return response()->json('User not found!', 404);
        }
        
        $year = Carbon::now()->format('Y');
        $month = Carbon::now()->format('m');
        $day = Carbon::now()->format('d');
        $time_in = Carbon::now()->format('H:i:s');

        if(Attendance::where(['user_id' => $user->id ,'year' => $year ,'month' => $month,'day' => $day,])->first())
        {
            return response()->json('You already login', 404);
        }
        else
        { 
        $attendance = new Attendance;
        $attendance->user_id = $user->id;
        $attendance->year = $year;
        $attendance->month = $month;
        $attendance->day = $day;
        $attendance->time_in = $time_in;
        $attendance->save();
        }

        return new AttendanceResource($attendance);  
    }

    public function update(Request $request)
    {   
        $attendance = Attendance::find($request->id);
        $time_out = Carbon::now()->format('H:i:s');
        $user = User::where('id', $attendance->user_id)->first();
        
        $total1 = Attendance::where('user_id', $user->id)->first();
        $attendance->time_out = $time_out;
        $start_time = strtotime($time_out);
        $end_time = strtotime($total1->time_in);
        $diff = $start_time-$end_time;
        $attendance->total = date('H:i:s', $diff);
        $attendance->save();
        
        return new AttendanceResource($attendance);
    }

    /**
     * @param User $user
     * @retun \Illuminate\Http\JsonResponse
     * @throws \Exception
     */

    public function destroy(Request $request)
    {   
        $attendance = Attendance::find($request->id);
        if(is_null($attendance)){
            return response()->json('Attendance not found!', 404);
        }
        $attendance->delete();
        return response()->json('Success!', 200);
    }
}
