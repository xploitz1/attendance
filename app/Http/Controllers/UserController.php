<?php

namespace App\Http\Controllers;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserResourceCollection;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use stdClass;
use Carbon\Carbon;



class UserController extends Controller
{
    /**
     * @param User $user
     * @return UserResource 
     */
    public function show(Request $request)
    {   
        // $limit = 10;
        // $user = User::where('first_name', 'like', '%' . $request->key . '%')->orWhere('last_name', 'like', '%' . $request->key . '%')->paginate($limit);
        $user = User::find($request->id);
        if(is_null($user)){
            return response()->json('User not found!', 404);
        }
        $user = new UserResource($user);
        return response()->json($user, 201);
    }
    /**
     * @return UserResourceCollection
     */
    public function index(Request $request)
    {   
        $limit = 10;

        if(isset($request->limit)) $limit = $request->limit;

        $user = User::where('status', !0)->orderBy('created_at', 'desc')->paginate($limit);
        $user = new UserResourceCollection($user);
        return response()->json($user, 201);
    }
    /**
     * @param Request $request
     * @return UserResource
     */

    public function store(Request $request)
    {
       $request->validate([
            'first_name'=> 'required',
            'last_name' => 'required',
            'mobile'    => 'required',
            'email'     => 'required|email|max:255',
            'password'  => 'required|min:6',
            'gender'    => 'required',
            'bdate'     => 'required',
            'address'   => 'required',
            'city'      => 'required',
            'type'      => 'required',
            'department'=> 'required',
        ]);

        do{
            $randomString = mt_rand(1,100000);
            $rand =  $randomString;
          }while(!empty(User ::where('employee_id',$rand)->first()));
        
        if(User::where('email', $request->email)->first()){
            return response()->json('The email has already been taken', 404);
        }
        
        $user = new User;
        $user->employee_id = $rand;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->password = bcrypt($request->password[0]);
        $user->gender = $request->gender;
        $user->bdate = $request->bdate;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->type = $request->type;
        $user->status = 1;
        $user->department = $request->department;
        $user->save();
        
        return new UserResource($user);
    }

    public function update(Request $request)
    {
        $request->validate([
            'first_name'=> 'required',
            'last_name' => 'required',
            'mobile'    => 'required',
            'email'     => 'required|email|max:255',
            'password'  => 'required|min:6',
            'gender'    => 'required',
            'bdate'     => 'required',
            'address'   => 'required',
            'city'      => 'required',
            'type'      => 'required',
            'department'=> 'required',
        ]);

        $user = User::find($request->id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->password = bcrypt($request->password[0]);
        $user->gender = $request->gender;
        $user->bdate = $request->bdate;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->type = $request->type;
        $user->status = 1;
        $user->department = $request->department;
        $user->save();

        return new UserResource($user);

    }
    /**
     * @param User $user
     * @retun \Illuminate\Http\JsonResponse
     * @throws \Exception
     */

    public function destroy(Request $request)
    {   
        $user = User::find($request->id);
        if(is_null($user)){
            return response()->json('User not found!', 404);
        }
        $user->delete();
        return response()->json('Success!', 200);
    }
}   