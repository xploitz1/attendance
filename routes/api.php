<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/user-index', 'UserController@index');
Route::post('/user', 'UserController@show');
Route::post('/user-store', 'UserController@store');
Route::post('/user-update', 'UserController@update');
Route::post('/user-delete', 'UserController@destroy');

Route::post('/attendance-index', 'AttendanceController@index');
Route::post('/attendance-show', 'AttendanceController@show');
Route::post('/attendance-login', 'AttendanceController@store');
Route::post('/attendance-logout', 'AttendanceController@update');
Route::post('/attendance-delete', 'AttendanceController@destroy');


 
