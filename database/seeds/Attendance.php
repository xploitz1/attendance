<?php

use Illuminate\Database\Seeder;

class Attendance extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Attendance::class, 50)->create();
    }
}
