<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'employee_id' => $faker->numberBetween(1,999),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'mobile' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => $faker->sha1, // password
        'remember_token' => Str::random(10),
        'gender' => $faker->randomElement($array = array ('male', 'female')),
        'bdate' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'address' => $faker->address,
        'city' => $faker->city,
        'department' => $faker->jobTitle,
        'status' => $faker->numberBetween(0,1),
        'type' => $faker->numberBetween(1,2),
    ];
});
